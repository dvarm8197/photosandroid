package com.example.daniel.photosandroid;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.daniel.photosandroid.model.Photo;

import java.io.File;
import java.util.ArrayList;

public class SlideshowActivity extends AppCompatActivity {

    int index = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //put the first pic in the imageview





        super.onCreate(savedInstanceState);
        setContentView(R.layout.slideshow);
        index = 0;
        setPic();
        //next
        Button nextButton = (Button) findViewById(R.id.Next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });
        //prev
        Button prevButton = (Button) findViewById(R.id.Prev);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prev();
            }
        });
        //exit
        Button exitButton = (Button) findViewById(R.id.Exit);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exit();
            }
        });
    }

    public void setPic(){
        ArrayList<Photo> photos = MainActivity.currentAlbum.getPhotos();
        Photo p = photos.get(index);

        try {

            File file = new File (p.getPath());
            if (file.exists()){
                System.out.println("FILE EXISTS");
                Bitmap newBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                ImageView image = (ImageView) findViewById(R.id.currPhoto);
                image.setImageBitmap(newBitmap);
            } else {
                System.out.println("FILE DOESNT EXIST");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void next(){
        if (index == MainActivity.currentAlbum.getPhotos().size() - 1){
            //out of bounds
            Toast.makeText(SlideshowActivity.this,"No next photo",Toast.LENGTH_LONG).show();
            return;
        }
        index++;
        setPic();
    }

    public void prev(){
        if (index == 0){
            //out of bounds
            Toast.makeText(SlideshowActivity.this,"No prev photo",Toast.LENGTH_LONG).show();
            return;
        }
        index--;
        setPic();
    }

    public void exit(){
        Intent photoIntent = new Intent(SlideshowActivity.this, PhotosDisplayActivity.class);
        startActivity(photoIntent);
    }
}
