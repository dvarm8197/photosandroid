package com.example.daniel.photosandroid;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.daniel.photosandroid.model.Photo;

import java.io.File;

public class SearchPhotos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_photos);
        populatePhotos();


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Search.result.clear();
    }
    public void populatePhotos(){

        LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.searchDisplay);

        //if (linearLayout1.)
        if (linearLayout1 != null) { //getChildCount
            linearLayout1.removeAllViews();
        }

        for(Photo p: Search.result){
            System.out.println("Populate photos path " + p.getPath());
            try {
                //FileInputStream f = new FileInputStream(p.getPath());
                String fileName = p.getName();
                String filePath = p.getPath();
                //figure out way to display name
                File file = new File (filePath);
                if (file.exists()){
                    final Photo tosend = p;
                    System.out.println("FILE EXISTS");
                    Bitmap newBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());

                    ImageView image = new ImageView(SearchPhotos.this);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(500,500);
                    image.setLayoutParams(lp);
                    image.setImageBitmap(newBitmap);


                    linearLayout1.addView(image);


                } else {
                    System.out.println("FILE DOESNT EXIST");
                }



            }catch(Exception e){
                e.printStackTrace();
            }

        }
    }
}
