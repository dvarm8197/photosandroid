package com.example.daniel.photosandroid;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.daniel.photosandroid.model.Album;
import com.example.daniel.photosandroid.model.Photo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class PhotosDisplayActivity extends AppCompatActivity {

    public static Photo currentPhoto = null;

    public void writeAlbum(ArrayList<Album> ar) throws IOException{
        //File file = new File("/MyFolder"+File.separator+"user.ser");
        File file = new File(getExternalFilesDir("/MyFolder"),"user.ser");
        //System.out.println("FILE PATH:  " + file.getAbsolutePath());
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
        oos.writeObject(ar);
        oos.close();
    }

    public ArrayList<Album> readAlbum(){
        // File file = new File("MyFolder"+File.separator+"user.ser");
        File file = new File(getExternalFilesDir("/MyFolder"),"user.ser");
        //System.out.println("FILE PATH:  " + file.getAbsolutePath());

        ArrayList<Album> albums = null;
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            albums = (ArrayList<Album>)ois.readObject();
            //ois.readObject();
            ois.close();
        } catch (IOException  | ClassNotFoundException c){
            c.printStackTrace();

        }
        return albums;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Here, thisActivity is the current activity

        if (ContextCompat.checkSelfPermission(PhotosDisplayActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            System.out.println("Permission not granted");
            // Permission is not granted
            // Should we show an explanation?

            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(PhotosDisplayActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    2);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.

        } else {
            // Permission has already been granted
        }


        populatePhotos();
        System.out.println(MainActivity.currentAlbum.getName());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.photos_display);
        populatePhotos();


        TextView tv = findViewById(R.id.textView2);
        tv.setText(MainActivity.currentAlbum.getName());
        //Add
        Button addButton = (Button) findViewById(R.id.Add);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photoAdd();
            }
        });

        //Remove

        //slideshow
        Button slideShow = (Button) findViewById(R.id.Slideshow);
        slideShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent slideIntent = new Intent(PhotosDisplayActivity.this, SlideshowActivity.class);
                startActivity(slideIntent);
            }
        });
    }

    public void popPhotos(){
        ArrayList<Photo> photos = MainActivity.currentAlbum.getPhotos();

        for (Photo p: photos ){
            String path = p.getPath();
            File file = new File(path);

        }
    }

    public void photoAdd(){
        System.out.println("In photo add");
        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI), 3);
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        //ImageView iw = findViewById(R.id.ImageHere);
        LinearLayout ll = findViewById(R.id.PhotoLayout);
        super.onActivityResult(requestCode, resultCode, data);
        Album newAlbum = null;
        if (requestCode == 3 && resultCode == Activity.RESULT_OK){
            final Uri selectedImage = data.getData();
            String path = getRealPathFromURI(PhotosDisplayActivity.this, selectedImage);//selectedImage.getPath();

            String name =  selectedImage.getLastPathSegment();
            System.out.println("File name: " + name);
            System.out.println(path);
            final Photo photo = new Photo(name, path);
            newAlbum = MainActivity.currentAlbum;
            //MainActivity.currentAlbum.getPhotos().add(photo); //adding to album here
            for (Photo p : newAlbum.getPhotos()){
                if (p.getPath().equals(path)){
                    System.out.println("DUPLICATE PICTURE");
                    Toast.makeText(PhotosDisplayActivity.this,"Duplicate",Toast.LENGTH_LONG).show();

                    return;
                }
            }


            newAlbum.getPhotos().add(photo);
            Bitmap bitmap = null;
            try {



                ImageView iw = new ImageView(PhotosDisplayActivity.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(500,500);
                iw.setLayoutParams(lp);
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                iw.setImageBitmap(bitmap);
                iw.setClickable(true);

                iw.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("ON CLICK LISTENER");
                        clickedImage(photo);
                    }
                });

                ll.addView(iw);

                System.out.println("adding imageview");
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }else{
            System.out.println("Clicked back button");
            return;
        }

        ReadWrite rw = new ReadWrite();
        MainActivity ma = new MainActivity();
        ArrayList<Album> albums = readAlbum(); //rw.readAlbum();
        int i = 0;
        for (Album a: albums){
            if (a.getName().equals(newAlbum.getName())){
                albums.set(i, newAlbum);
            }
            i++;

        }
        try {
            writeAlbum(albums);
        } catch (IOException e){
            e.printStackTrace();
        }

    }

    public void clickedImage (Photo p){ //on click listener for the image

        System.out.println("Clicked image " + p.getPath());
        //final String [] options = {"Open", "Delete"};
        final String [] options = {"Display", "Delete", "Move"};
        final Photo toDelete = p;
        AlertDialog.Builder build = new AlertDialog.Builder(PhotosDisplayActivity.this)
                .setSingleChoiceItems(options, 0, null)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i){
                        int selected = ((AlertDialog)dialogInterface).getListView().getCheckedItemPosition();

                        switch (selected){
                            case 0:
                                //TODO Open method and link to new view
                                System.out.println("OPEN");
                                currentPhoto = toDelete;
                                openImage(toDelete);
                                break;
                            case 1:
                                System.out.println("DELETE");
                                deleteImage(toDelete);
                                break;
                            case 2:
                                //TODO Move method
                                moveImage(toDelete);
                                break;
                        }

                    }
                })
                .setNegativeButton("Cancel", null);
        build.setTitle("Select option");
        build.show();
    }

    public void moveImage(final Photo photo){
        final ArrayList<Album> albums = readAlbum();
        final String [] albumNames = new String [albums.size()];
        int i = 0;
        for (Album a : albums){
            albumNames[i] = a.getName();
            i++;
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(PhotosDisplayActivity.this)
                .setSingleChoiceItems(albumNames, 0, null)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int selected = ((AlertDialog)dialogInterface).getListView().getCheckedItemPosition();
                        String toAlbum = albumNames[selected];
                        System.out.println("MOVING TO " + toAlbum);

                        String currAlbum = MainActivity.currentAlbum.getName();

                        if (toAlbum.equals(currAlbum) ){ //do nothing
                            Toast.makeText(PhotosDisplayActivity.this,"This photo is already in that album",Toast.LENGTH_SHORT).show();
                            return;
                        }

                        //adding to new album
                        ArrayList<Album> newAlbum = readAlbum();
                        for (Album a : newAlbum){
                            if (a.getName().equals(toAlbum)){
                                a.getPhotos().add(photo);
                            }
                        }

                        try {
                            writeAlbum(newAlbum);
                        } catch (IOException e){
                            e.printStackTrace();
                        }


                        deleteImage(photo);
                        Toast.makeText(PhotosDisplayActivity.this,"Photo moved to " + toAlbum,Toast.LENGTH_SHORT).show();


                    }
                })
                .setNegativeButton("Cancel", null);

        builder.setTitle("Select album to move to");
        builder.show();
    }

    public void openImage (Photo photo){
        Intent photoIntent = new Intent(PhotosDisplayActivity.this, IndividualPhotoActivity.class);
        startActivity(photoIntent);
    }

    public void deleteImage(Photo photo){
        Album newAlbum = MainActivity.currentAlbum;

        ArrayList<Photo> photos = MainActivity.currentAlbum.getPhotos();

        Photo toRemove = null;
        for (Photo p : newAlbum.getPhotos()){
            if (p.getPath().equals(photo.getPath())){
                //deleting
                toRemove = p;
                //newAlbum.getPhotos().remove(p);
            }
        }
        newAlbum.getPhotos().remove(toRemove);

        ArrayList<Album> albums = readAlbum(); //rw.readAlbum();
        int i = 0;
        for (Album a: albums){
            if (a.getName().equals(newAlbum.getName())){
                albums.set(i, newAlbum);
            }
            i++;

        }
        try {
            writeAlbum(albums);
        } catch (IOException e){
            e.printStackTrace();
        }



        populatePhotos();
    }

    public void populatePhotos(){

        LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.PhotoLayout);

        //if (linearLayout1.)
        if (linearLayout1 != null) { //getChildCount
            linearLayout1.removeAllViews();
        }
        if (MainActivity.currentAlbum.getPhotos() == null){
            return;
        }
        for(Photo p: MainActivity.currentAlbum.getPhotos()){
            System.out.println("Populate photos path " + p.getPath());
            try {
                //FileInputStream f = new FileInputStream(p.getPath());
                String fileName = p.getName();
                String filePath = p.getPath();
                //figure out way to display name
                File file = new File (filePath);
                if (file.exists()){
                    final Photo tosend = p;
                    System.out.println("FILE EXISTS");
                    Bitmap newBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());

                    ImageView image = new ImageView(PhotosDisplayActivity.this);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(500,500);
                    image.setLayoutParams(lp);
                    image.setImageBitmap(newBitmap);

                    image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            System.out.println("ON CLICK LISTENER");
                            clickedImage(tosend);
                        }
                    });

                    linearLayout1.addView(image);


                } else {
                    System.out.println("FILE DOESNT EXIST");
                }

            }catch(Exception e){
                e.printStackTrace();
            }

        }
    }

    public void addPhoto(){
        //open file explorer
        //int PICKFILE_RESULT_CODE=1;
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(intent, 7);

    }




}

