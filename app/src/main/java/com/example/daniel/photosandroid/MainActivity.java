package com.example.daniel.photosandroid;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.daniel.photosandroid.model.Album;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class  MainActivity extends AppCompatActivity {

    public static ArrayList<Album> albumsMain = new ArrayList<>();
    public static Album currentAlbum = null;//new Album();

    public void writeAlbum(ArrayList<Album> ar) throws IOException{
        //File file = new File("/MyFolder"+File.separator+"user.ser");
        File file = new File(getExternalFilesDir("/MyFolder"),"user.ser");
        //System.out.println("FILE PATH:  " + file.getAbsolutePath());
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
        oos.writeObject(ar);
        oos.close();
    }

    public ArrayList<Album> readAlbum(){
        // File file = new File("MyFolder"+File.separator+"user.ser");
        File file = new File(getExternalFilesDir("/MyFolder"),"user.ser");
        //System.out.println("FILE PATH:  " + file.getAbsolutePath());

        ArrayList<Album> albums = null;
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            albums = (ArrayList<Album>)ois.readObject();
            //ois.readObject();
            ois.close();
        } catch (IOException  | ClassNotFoundException c){
            c.printStackTrace();

        }
        return albums;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //File file = new File(getExternalFilesDir("/MyFolder"),"user.ser");

        if (fileExist( "user.ser")){
            System.out.println("Exists");

        } else {
            System.out.println("Dont exist");
            File file = new File(getExternalFilesDir("/MyFolder"),"user.ser");
            try{
                FileOutputStream fos = new FileOutputStream(file);
                fos.close();
            }catch(IOException e){
                e.printStackTrace();
            }

        }


        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)){

        }else{
            Toast.makeText(this,"cant",Toast.LENGTH_SHORT).show();
        }
        System.out.println("Test2");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        view();

        //search button
        Button searchButton = (Button) findViewById(R.id.search);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentSearch = new Intent(MainActivity.this, Search.class);
                startActivity(intentSearch);
            }
        });

        //create button
        Button createButton = (Button) findViewById(R.id.create);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Enter album name");

                final EditText input = new EditText(MainActivity.this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                //String in = "";

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        in = input.getText().toString();

                        albumsMain = readAlbum();
                        if(albumsMain == null){
                            albumsMain = new ArrayList<Album>();
                        }
                        //ArrayList<Album> albums = readAlbum(); //read this instead of new
                        Album a = new Album(in);
                        albumsMain.add(a);
                        try {
                            writeAlbum(albumsMain);
                        } catch (IOException e){
                            e.printStackTrace();
                        }
                        view(); //refreshes UI with new album
                        System.out.println("Input: " + in);
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                builder.show();


            }
        });




        view();
        System.out.println("Input2: " + in);
    }

    public Album getAlbum(String name){
        ArrayList<Album> albums = readAlbum();
        for (Album i: albums){
            if (name.equals(i.getName())){
                return i;
            }
        }
        return null;
    }

    public boolean duplicate(String name){
        albumsMain = readAlbum();
        for (Album a : albumsMain){
            if (a.getName().equals(name)){
                return true;
            }
        }
        return false;
    }

    public void deleteAlbum(Album name){
        albumsMain = readAlbum();

        Album toDelete = null;
        for (Album a : albumsMain){
            if (a.getName().equals(name.getName())){
                //albumsMain.remove(a);
                toDelete = a;
                System.out.println("SUCCSSFUL DELETE");
                view();
            } else {
                System.out.println("UNSUCCESFUL DELETE");
            }
        }
        albumsMain.remove(toDelete);

        try {
            writeAlbum(albumsMain);
        }catch(IOException e){
            e.printStackTrace();
        }
        view();

    }

    public void renameAlbum(Album name){
        System.out.println("RENAMING");
        albumsMain = readAlbum();
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Enter new name");

        final EditText input = new EditText(MainActivity.this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        final String albumName = name.getName();
        //String rename  = "";
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String rename = input.getText().toString();
                if (duplicate(rename)){
                    Toast.makeText(MainActivity.this, "ERROR: Another album with that name exists", Toast.LENGTH_SHORT).show();
                    return;
                }
                Album toRename = null;
                for (Album i : albumsMain){
                    if (i.getName().equals(albumName)){
                        System.out.println("ACTUALLY RENAMING");
                        toRename = i;
                        i.setName(rename);
                    } else {
                        System.out.println("NOT RENMAING");
                    }
                }
                dialog.dismiss();

                try {
                    writeAlbum(albumsMain);
                } catch (IOException e){
                    e.printStackTrace();
                }

                view();
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.show();
        view();
    }

    //TODO https://stackoverflow.com/questions/10696986/how-to-set-the-part-of-the-text-view-is-clickable -add text thats clickable
    public void view(){
        ListView lv = findViewById(R.id.listView);

        ArrayList<Album> albums = readAlbum();


        if (albums == null || albums.isEmpty()){
            System.out.println("Albums null or empty");
            ArrayAdapter<String> blank = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new ArrayList<String>());
            lv.setAdapter(blank);
            return;
        }

        ArrayList<String> albumNames = new ArrayList<>();

        for (Album i: albums){
            System.out.println(i.getName());
            albumNames.add(i.getName());
            // tw.setText(i.getName());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, albumNames);

        lv.setAdapter(adapter);
        lv.setClickable(true);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ListView vi = findViewById(R.id.listView);

                final String albumName = (String) vi.getItemAtPosition(i);
                //Toast.makeText(MainActivity.this,"You selected : " + item,Toast.LENGTH_SHORT).show();*/
                final String [] options = {"Open", "Delete", "Rename"};
                int checkedItem = 0;
                String curr = "";

                AlertDialog.Builder build = new AlertDialog.Builder(MainActivity.this)
                        .setSingleChoiceItems(options, 0, null)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                int selected = ((AlertDialog)dialogInterface).getListView().getCheckedItemPosition();
                                switch (selected){
                                    case 0: //open
                                        Intent photoIntent = new Intent(MainActivity.this, PhotosDisplayActivity.class);
                                        currentAlbum = getAlbum(albumName);
                                        startActivity(photoIntent);
                                        break;
                                    case 1: //delete
                                        Album toDelete = getAlbum(albumName);
                                        deleteAlbum(toDelete);
                                        view();
                                        break;
                                    case 2:    //rename
                                        Album toRename = getAlbum(albumName);
                                        renameAlbum(toRename);
                                        view();
                                        break;
                                }

                            }
                        })
                        .setNegativeButton("Cancel", null);
                build.setTitle("Choose option");

                build.show();

            }
        });

    }


    String in = "";


    public boolean fileExist(String fname){
        String path = "/storage/emulated/0/Android/data/com.example.daniel.photos/files/MyFolder/user.ser";
        //Environment.
        File f = new File(path);
        if (f.exists()){
            return true;
        } else {
            return false;
        }

    }




}
