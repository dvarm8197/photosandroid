package com.example.daniel.photosandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.daniel.photosandroid.model.Album;
import com.example.daniel.photosandroid.model.Photo;
import com.example.daniel.photosandroid.model.Tag;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class Search extends AppCompatActivity {

    static ArrayList<Photo> result = new ArrayList<Photo>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);

        result = new ArrayList<Photo>();
        //initialize drop downs
        //make sure values are disable until they select a name
        //distinguish between single tag or multi tag

        Spinner andor = (Spinner) findViewById(R.id.andor);
        String[] items = new String[]{"", "and", "or"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        andor.setAdapter(adapter);


        String[] tagNames = new String[]{"", "People", "Location"};
        ArrayAdapter<String> tnames = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, tagNames);

        Spinner tn1 = (Spinner) findViewById(R.id.TagName1);
        tn1.setAdapter(tnames);

        tn1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!parent.getSelectedItem().toString().equals("")) {
                    findViewById(R.id.TagValue1).setFocusableInTouchMode(true);
                }else{
                    findViewById(R.id.TagValue1).setFocusable(false);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner tn2 = (Spinner) findViewById(R.id.TagName2);
        tn2.setAdapter(tnames);

        tn2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!parent.getSelectedItem().toString().equals("")) {
                    findViewById(R.id.TagValue2).setFocusableInTouchMode(true);
                }else{
                    findViewById(R.id.TagValue2).setFocusable(false);

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        EditText tv1 = (EditText) findViewById(R.id.TagValue1);
        EditText tv2 = (EditText) findViewById(R.id.TagValue2);
        tv1.setFocusable(false);
        tv2.setFocusable(false);

        final Button search = (Button) findViewById(R.id.doSearch);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });


    }



    public void search(){
        EditText tv1 = (EditText) findViewById(R.id.TagValue1);
        EditText tv2 = (EditText) findViewById(R.id.TagValue2);
        Spinner tn1 = (Spinner) findViewById(R.id.TagName1);
        Spinner tn2 = (Spinner) findViewById(R.id.TagName2);
        Spinner andor = (Spinner) findViewById(R.id.andor);
        boolean multi = false;

        //check if tn2 if filled, if not then check andor
        if(tn2.getSelectedItem().toString().equals("")){
            if(!andor.getSelectedItem().toString().equals("")){
                //select tagname 2 please
                Toast.makeText(this, "Please fill in Tag Name 2", Toast.LENGTH_LONG).show();
                return;
            }else{
                //check tagname 1 and tagvalue 1
                if(tn1.getSelectedItem().equals("")){
                    //select tagname 1 please
                    Toast.makeText(this, "Please fill in Tag Name 1", Toast.LENGTH_LONG).show();
                    return;
                }else if(tv1.getText().toString().equals("")){
                    //fill in tagvalue 1
                    Toast.makeText(this, "Please fill in Tag Value 1", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }else{
            if(andor.getSelectedItem().toString().equals("")){
                //select conjunction
                Toast.makeText(this, "Please fill in and/or", Toast.LENGTH_LONG).show();
                return;
            }
            if(tv2.getText().toString().equals("")){
                //fill in tagvalue 2
                Toast.makeText(this, "Please fill in Tag Value 2", Toast.LENGTH_LONG).show();
                return;
            }else{
                //check tagname 1 and tagvalue 1
                if(tn1.getSelectedItem().equals("")){
                    //select tagname 1 please
                    Toast.makeText(this, "Please fill in Tag Name 1", Toast.LENGTH_LONG).show();
                    return;
                }else if(tv1.getText().toString().equals("")){
                    //fill in tagvalue 1
                    Toast.makeText(this, "Please fill in Tag Value 1", Toast.LENGTH_LONG).show();
                    return;
                }
            }
            multi = true;
        }




        //for every album go through photos
        if(multi){
            multiTag();
        }else{
            singleTag();
        }



        //pass that photo list somewhere to display it
    }

    public ArrayList<Album> readAlbum(){
        // File file = new File("MyFolder"+File.separator+"user.ser");
        File file = new File(getExternalFilesDir("/MyFolder"),"user.ser");
        //System.out.println("FILE PATH:  " + file.getAbsolutePath());

        ArrayList<Album> albums = null;
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            albums = (ArrayList<Album>)ois.readObject();
            //ois.readObject();
            ois.close();
        } catch (IOException | ClassNotFoundException c){
            c.printStackTrace();

        }
        return albums;
    }

    public void multiTag(){
        EditText tv1 = (EditText) findViewById(R.id.TagValue1);
        EditText tv2 = (EditText) findViewById(R.id.TagValue2);
        Spinner tn1 = (Spinner) findViewById(R.id.TagName1);
        Spinner tn2 = (Spinner) findViewById(R.id.TagName2);
        Spinner andor = (Spinner) findViewById(R.id.andor);

        String con = andor.getSelectedItem().toString();
        ArrayList<Album> albums = readAlbum();

        for(Album a : albums){
            for(Photo p : a.getPhotos()){
                for(Tag t : p.getTags()){
                    if(con.equals("and")) {
                        if (t.getTagName().equals(tn1.getSelectedItem().toString())&& t.getTagValue().contains(tv1.getText().toString())){
                            for(Tag t2 : p.getTags()){
                                if(t2.getTagName().equals(tn2.getSelectedItem().toString())&&t2.getTagValue().contains(tv2.getText().toString())){
                                    Photo deepCopy = new Photo(p.getName(),p.getPath());
                                    p.setTags(p.getTags());
                                    result.add(deepCopy);
                                }
                            }
                        }
                    }else if(con.equals("or")){
                        if((t.getTagName().equals(tn1.getSelectedItem().toString())&&t.getTagValue().contains(tv1.getText().toString())
                        )||(t.getTagName().equals(tn2.getSelectedItem().toString())&&t.getTagValue().contains(tv2.getText().toString()))){
                            Photo deepCopy = new Photo(p.getName(),p.getPath());
                            p.setTags(p.getTags());
                            result.add(deepCopy);
                        }
                    }
                }
            }
        }

        Intent searchIntent = new Intent(Search.this, SearchPhotos.class);
        startActivity(searchIntent);
    }

    public void singleTag(){
        //going to need every album
        // Toast.makeText(this, "in single", Toast.LENGTH_SHORT).show();
        EditText tv1 = (EditText) findViewById(R.id.TagValue1);
        Spinner tn1 = (Spinner) findViewById(R.id.TagName1);
        //Toast.makeText(this,tn1.getSelectedItem().toString()+" " + tv1.getText().toString(), Toast.LENGTH_SHORT).show();

        ArrayList<Album> albums = readAlbum();
        for(Album a : albums){

            for(Photo p : a.getPhotos()){
                for(Tag t : p.getTags()){
                    Toast.makeText(this, t.getTagName()+" "+ t.getTagValue(), Toast.LENGTH_SHORT).show();

                    if(tn1.getSelectedItem().toString().equals(t.getTagName())
                            &&t.getTagValue().contains(tv1.getText().toString())){
                        // Toast.makeText(this, p.getName(), Toast.LENGTH_SHORT).show();
                        Photo deepCopy = new Photo(p.getName(),p.getPath());
                        p.setTags(p.getTags());
                        result.add(deepCopy);
                    }
                }
            }
        }
        Intent searchIntent = new Intent(Search.this, SearchPhotos.class);
        startActivity(searchIntent);
    }

}
