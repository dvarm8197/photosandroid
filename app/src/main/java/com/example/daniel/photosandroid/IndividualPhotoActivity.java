package com.example.daniel.photosandroid;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.daniel.photosandroid.model.Album;
import com.example.daniel.photosandroid.model.Photo;
import com.example.daniel.photosandroid.model.Tag;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class IndividualPhotoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.individual_photo);


        //setting the image view with the current photo
        ImageView iv = findViewById(R.id.individualPhotoImage);
        Photo curr = PhotosDisplayActivity.currentPhoto;

        //displaing the file
        File f = new File((curr.getPath()));
        if (f.exists()){
            System.out.println("FILE EXISTS IN INDIVIDUAL");
            Bitmap bm = BitmapFactory.decodeFile(f.getAbsolutePath());
            iv.setImageBitmap(bm);
        } else {
            System.out.println("FILE DOESN't exist in INDIVIDUAL");
        }

        populateView();

        //add tag button
        Button addTagB = findViewById(R.id.addTag);
        addTagB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("CLCIKING add tag");
                addTagClick();
            }
        });

        Button deleteTagB = findViewById(R.id.deleteTag);
        deleteTagB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteTagClick();
            }
        });
    }

    public void deleteTagClick(){
        ArrayList<Album> albums = readAlbum();
        Album currAlbum = MainActivity.currentAlbum;
        Photo currPhoto = PhotosDisplayActivity.currentPhoto;

        ArrayList<Tag> tags = null;

        for (Album a : albums){
            if (a.getName().equals(currAlbum.getName())) {
                for (Photo p : a.getPhotos()) {
                    if (p.getName().equals(currPhoto.getName())){
                        tags = p.getTags();
                    }
                }
            }
        }

        final String [] tagNames = new String [tags.size()];
        int i = 0;
        for (Tag t : tags){
            tagNames [i] = t.getTagName() + ": " + t.getTagValue();
            i++;
        }
        final ArrayList<Tag> newTagList = tags;
        AlertDialog.Builder builder = new AlertDialog.Builder(IndividualPhotoActivity.this)
                .setSingleChoiceItems(tagNames, 0, null)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int selected = ((AlertDialog)dialogInterface).getListView().getCheckedItemPosition();
                        System.out.println("DELETING " + tagNames[selected]);
                        newTagList.remove(selected);
                        updateDeletedTag(newTagList);
                    }
                })
                .setNegativeButton("Cancel", null)
                .setTitle("Choose which tag to delete");

        builder.show();
    }

    public void updateDeletedTag(ArrayList<Tag> in){
        ArrayList<Album> albums = readAlbum();
        Album currAlbum = MainActivity.currentAlbum;
        Photo currPhoto = PhotosDisplayActivity.currentPhoto;

        for (Album a : albums) {
            if (a.getName().equals(currAlbum.getName())) {
                for (Photo p : a.getPhotos()) {
                    if (p.getName().equals(currPhoto.getName())) {
                        p.setTags(in);
                    }
                }
            }
        }

        try {
            writeAlbum(albums);
        } catch (IOException e){
            e.printStackTrace();
        }
        populateView();

    }

    public void addTagClick(){
        final String [] options = {"People", "Location"};

        AlertDialog.Builder builder = new AlertDialog.Builder(IndividualPhotoActivity.this)
                .setSingleChoiceItems(options, 0 , null)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int selected = ((AlertDialog)dialogInterface).getListView().getCheckedItemPosition();
                        if (selected == 0){
                            textInput("People");
                        } else if (selected == 1){
                            textInput("Location");
                        }
                    }
                })
                .setNegativeButton("Cancel", null);
        builder.setTitle("Choose type of tag you want to add");
        builder.show();
    }

    public void textInput( String tagName){
        //TODO check if location tag already exists for photo, and if it does just return
        final String name = tagName;
        AlertDialog.Builder builder = new AlertDialog.Builder(IndividualPhotoActivity.this);
        builder.setTitle("Enter tag name");

        final EditText input = new EditText(IndividualPhotoActivity.this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String in = input.getText().toString();
                System.out.println("INPUT OF TEXT: " + in);
                addTag(name, in);
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.setTitle("Enter value of tag");

        builder.show();
    }

    public void populateView(){
        ArrayList<Album> albums = readAlbum();
        Album currAlbum = MainActivity.currentAlbum;
        Photo currPhoto = PhotosDisplayActivity.currentPhoto;

        ArrayList<Tag> tags = null;

        for (Album a : albums){
            if (a.getName().equals(currAlbum.getName())) {
                for (Photo p : a.getPhotos()) {
                    if (p.getName().equals(currPhoto.getName())){
                        tags = p.getTags();
                    }
                }
            }
        }

        ArrayList<String> tagsList = new ArrayList<>();
        for (Tag t : tags){
            System.out.println("Tag: " + t.getTagName() + " " + t.getTagValue());
            tagsList.add(t.getTagName() + ": " + t.getTagValue());
        }

        ListView listView = findViewById(R.id.tagsList);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, tagsList);
        listView.setAdapter(adapter);
        listView.setClickable(false);

    }

    public void addTag(String tagName, String tagValue){
        Tag toAdd = new Tag(tagName, tagValue);
        ArrayList<Album> albums = readAlbum();
        Album currAlbum = MainActivity.currentAlbum;
        ArrayList<Photo> photos = currAlbum.getPhotos();
        Photo currPhoto = PhotosDisplayActivity.currentPhoto;

        ArrayList<Album> newAlbum = albums;
        int i = 0;
        int x = 0;
        for (Album a: albums){
            if (a.getName().equals(currAlbum.getName())){
                //in current album, now get curr photo
                for (Photo p: a.getPhotos()){
                    if (p.getName().equals(currPhoto.getName())){

                        ArrayList<Tag> tags = p.getTags();
                        for (Tag t: tags){
                            if (t.getTagValue().equals(tagValue) && t.getTagName().equals(tagName)){ //duplicate
                                Toast.makeText(IndividualPhotoActivity.this,"Duplicate tag", Toast.LENGTH_SHORT).show();
                                return;
                            } else if (t.getTagName().equals("Location") && tagName.equals("Location")){
                                Toast.makeText(IndividualPhotoActivity.this,"Location tag already exists", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }

                        System.out.println("ADDING TAG HERE");

                        //p.addTag(toAdd);
                        albums.get(i).getPhotos().get(x).addTag(toAdd);
                        MainActivity.currentAlbum.getPhotos().get(x).addTag(toAdd);
                        try {
                            writeAlbum(albums);
                        } catch (IOException e){
                            e.printStackTrace();
                        }
                        populateView();
                    }
                    x++;
                }
            }
            i++;
        }


    }





    public void writeAlbum(ArrayList<Album> ar) throws IOException {
        //File file = new File("/MyFolder"+File.separator+"user.ser");
        File file = new File(getExternalFilesDir("/MyFolder"),"user.ser");
        //System.out.println("FILE PATH:  " + file.getAbsolutePath());
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
        oos.writeObject(ar);
        oos.close();
    }

    public ArrayList<Album> readAlbum(){
        // File file = new File("MyFolder"+File.separator+"user.ser");
        File file = new File(getExternalFilesDir("/MyFolder"),"user.ser");
        //System.out.println("FILE PATH:  " + file.getAbsolutePath());

        ArrayList<Album> albums = null;
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            albums = (ArrayList<Album>)ois.readObject();
            //ois.readObject();
            ois.close();
        } catch (IOException  | ClassNotFoundException c){
            c.printStackTrace();

        }
        return albums;
    }
}
