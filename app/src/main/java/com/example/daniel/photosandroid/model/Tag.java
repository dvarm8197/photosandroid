package com.example.daniel.photosandroid.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Tag implements Serializable {
    String tagName;
    String tagValue;

    public Tag (String name, String value){
        tagName = name;
        tagValue = value;
    }


    static ArrayList<String> preLoadedTags = new ArrayList<String>();

    public void generateDefaultTag(){
        preLoadedTags.add("Location");
        preLoadedTags.add("Person");
    }

    /**
     *
     * @return name of tag
     */
    public String getTagName(){return tagName;}


    /**
     *
     * @return value of the tag
     */
    public String getTagValue(){return tagValue;}

    /**
     *
     * @param s name of tag
     */
    public void setTagName(String s){ tagName = s;}

    /**
     *
     * @param s value of tag
     */
    public void setTagValue(String s) { tagValue = s;}

}
