package com.example.daniel.photosandroid;

import android.support.v7.app.AppCompatActivity;

import com.example.daniel.photosandroid.model.Album;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class ReadWrite extends AppCompatActivity {

    public ArrayList<Album> readAlbum(){
        // File file = new File("MyFolder"+File.separator+"user.ser");
        File file = new File(getExternalFilesDir("/MyFolder"),"user.ser");
        System.out.println("FILE PATH:  " + file.getAbsolutePath());

        ArrayList<Album> albums = null;
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            albums = (ArrayList<Album>)ois.readObject();
            //ois.readObject();
            ois.close();
        } catch (IOException  | ClassNotFoundException c){
            c.printStackTrace();

        }
        return albums;
    }

    public void writeAlbum(ArrayList<Album> ar) throws IOException{
        //File file = new File("/MyFolder"+File.separator+"user.ser");
        File file = new File(getExternalFilesDir("/MyFolder"),"user.ser");
        System.out.println("FILE PATH:  " + file.getAbsolutePath());
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
        oos.writeObject(ar);
        oos.close();
    }


}
